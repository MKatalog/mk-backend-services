using Microsoft.AspNetCore.Mvc;
using PlatanParsingService.Services;

namespace PlatanParsingService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        private readonly PlatanParserService _platanParserService;

        public TestController(PlatanParserService platanParserService)
        {
            _platanParserService = platanParserService;
        }

        [HttpGet]
        public async Task<IActionResult> TestParsing()
        {
            try
            {
                await _platanParserService.ParsePlanatDataAsync();
                return Ok("Parsing successful.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }
    }
}
