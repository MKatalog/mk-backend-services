using System.Text.RegularExpressions;
using AngleSharp.Html.Parser;
using DotNetEnv;
using MKatalog.Abstractions.Models;
using MKatalog.Abstractions.Models.Dto;
using PlatanParsingService.Services.Kafka;

namespace PlatanParsingService.Services;
public class PlatanParserService
{
    private readonly HttpClient _httpClient;
    private readonly HtmlParser _parser;
    private readonly KafkaRequestResponseHandler<Category, Category> _kafkaCategoryHandler;
    private readonly KafkaRequestResponseHandler<Product, Product> _kafkaProductHandler;
    private readonly KafkaRequestResponseHandler<ProductLinkDto, ProductLinkDto> _kafkaProductLinkHandler;
    private readonly KafkaRequestResponseHandler<Characteristic, Characteristic> _kafkaCharacteristicHandler;

    private readonly string _categoryRequestTopic;
    private readonly string _categoryResponseTopic;

    private readonly string _productRequestTopic;
    private readonly string _productResponseTopic;

    private readonly string _productLinkRequestTopic;
    private readonly string _productLinkResponseTopic;
    
    private readonly string _characteristicRequestTopic;
    private readonly string _characteristicResponseTopic;

    private readonly string _bootstrapServers;
    private readonly string _platanUrl;
    private readonly int _maxRetryAttempts; //TODO  убрать


    public PlatanParserService(IHttpClientFactory httpClientFactory,
        IConfiguration configuration,
        HtmlParser parser)
    {
        _maxRetryAttempts = 20; //TODO  убрать

        _platanUrl = configuration.GetValue<string>("Urls:PlaranUrl") 
            ?? throw new InvalidOperationException("Platan URL is not configured.");

        _httpClient = httpClientFactory.CreateClient();
        _parser = parser;

        _bootstrapServers = Env.GetString("KAFKA_BOOTSTRAP_SERVERS");

        _categoryRequestTopic = Env.GetString("KAFKA_CATEGORY_REQUEST_TOPIC");
        _categoryResponseTopic = Env.GetString("KAFKA_CATEGORY_RESPONSE_TOPIC");

        _productRequestTopic = Env.GetString("KAFKA_PRODUCT_REQUEST_TOPIC");
        _productResponseTopic = Env.GetString("KAFKA_PRODUCT_RESPONSE_TOPIC");

        _productLinkRequestTopic = Env.GetString("KAFKA_PRODUCTLINK_REQUEST_TOPIC");
        _productLinkResponseTopic = Env.GetString("KAFKA_PRODUCTLINK_RESPONSE_TOPIC");

        _characteristicRequestTopic = Env.GetString("KAFKA_CHARACTERISTIC_REQUEST_TOPIC");
        _characteristicResponseTopic = Env.GetString("KAFKA_CHARACTERISTIC_RESPONSE_TOPIC");

        _kafkaCategoryHandler = new KafkaRequestResponseHandler<Category, Category>(_bootstrapServers, _categoryRequestTopic, _categoryResponseTopic);
        _kafkaProductHandler = new KafkaRequestResponseHandler<Product, Product>(_bootstrapServers, _productRequestTopic, _productResponseTopic);
        _kafkaProductLinkHandler = new KafkaRequestResponseHandler<ProductLinkDto, ProductLinkDto>(_bootstrapServers, _productLinkRequestTopic, _productLinkResponseTopic);
        _kafkaCharacteristicHandler = new KafkaRequestResponseHandler<Characteristic, Characteristic>(_bootstrapServers, _characteristicRequestTopic, _characteristicResponseTopic);
    }
    public async Task ParsePlanatDataAsync()
    {
        var htmlContent = await _httpClient.GetStringAsync(_platanUrl + "/shop/?group=2");
        var document = await _parser.ParseDocumentAsync(htmlContent);

        var headerElement = document.QuerySelector(".col-12 h1");
        var parentCategoryName = headerElement?.TextContent?.Trim();

        if (string.IsNullOrEmpty(parentCategoryName))
            return;
        var parentCategory = await PostgreSQLHelper.DatabaseService.GetCategoryByNameAsync(parentCategoryName);// ?? await _kafkaCategoryHandler.SendRequestAndWaitForResponseAsync(new Category { Name = parentCategoryName});
        var categoryGroupList = document.QuerySelectorAll(".col-sm-6.col-md-6");

        foreach (var categoryGroup in categoryGroupList)
        {
            var categoryName = categoryGroup.QuerySelector("h4")?.TextContent.Trim();
            if (string.IsNullOrEmpty(categoryName))
                continue;
            var category = await PostgreSQLHelper.DatabaseService.GetCategoryByNameAsync(categoryName);// ?? await _kafkaCategoryHandler.SendRequestAndWaitForResponseAsync(new Category { 
                //     Name = categoryName,
                //     ParentId = parentCategory?.Id
                // });
            var localCategoryList = categoryGroup.QuerySelectorAll(".col-12 a");

            foreach (var localCategoryElem in localCategoryList)
            {
                var subcategoryNameShit = localCategoryElem.TextContent.Trim();
                var regex = new Regex(@"\(\d+\)");
                var subcategoryName = regex.Replace(subcategoryNameShit, "").Trim();

                if (string.IsNullOrEmpty(subcategoryName))
                    continue;
                
                var subcategory = await PostgreSQLHelper.DatabaseService.GetCategoryByNameAsync(subcategoryName);

                // if (subcategory == null)
                // {
                //     subcategory = await _kafkaCategoryHandler.SendRequestAndWaitForResponseAsync(new Category {
                //         Name = subcategoryName,
                //         ParentId = category?.Id
                //     });
                // }
                var link = localCategoryElem.GetAttribute("href");
                

                if (link != null)
                    await ParseProductGroupAsync(_platanUrl + link, subcategory);
            }
        }
    }
    public async Task ParseProductGroupAsync(string url, Category category)
    {   
        await ParsePageAsync(url, category);

        var currentPageUrl = url;

        var htmlContent = await _httpClient.GetStringAsync(currentPageUrl);
        var document = await _parser.ParseDocumentAsync(htmlContent);

        var pagination = document.QuerySelector("#pagination1");
        if (pagination != null)
        {
            var nextPageLinks = pagination?.QuerySelectorAll("a");
            int k = 0;
            foreach (var nextPage in nextPageLinks)
            {                
                currentPageUrl = nextPage?.GetAttribute("href");
                
                if (!string.IsNullOrEmpty(currentPageUrl))
                {
                    try
                    {

                        await ParsePageAsync(_platanUrl + "cgi-bin/qweryv.pl/" + currentPageUrl, category);
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine($"Ошибка: {ex.Message}");
                    }
                }
                k++;
            }
        }
    }

    private async Task ParsePageAsync(string url, Category category)
    {
        var htmlContent = await _httpClient.GetStringAsync(url);
        var document = await _parser.ParseDocumentAsync(htmlContent);
        
        var productGrid = document.QuerySelector(".row.m-0.m-md-n3");
        var productCardList = productGrid?.QuerySelectorAll(".col-12.col-md-3.mb-2.p-0");

        if (productCardList?.Length != 0 && productCardList != null)
        {
            foreach (var productCard in productCardList)
            {
                var productUrl = productCard.QuerySelector("a")?.GetAttribute("href");

                if (!string.IsNullOrEmpty(productUrl))
                {
                    await ParseProductLinkAsync(_platanUrl + productUrl, category);
                }
            }
        }
    }
    private async Task ParseProductLinkAsync(string productUrl, Category category)
    {
        var htmlContent = await _httpClient.GetStringAsync(productUrl);
        var document = _parser.ParseDocument(htmlContent);

        var productContainer = document.QuerySelector(".container .row[itemtype='http://schema.org/Product']");
        var productNameElement = productContainer?.QuerySelector("h1[itemprop='name']");

        var productNameDescription = productNameElement?.TextContent.Trim();

        var productName = string.Empty;
        var productDescription = string.Empty;

        if (productNameDescription != null && productNameDescription.Contains(','))
        {
            string[] parts =  productNameDescription.Split(',');

            productName = parts[0].Trim();
            productDescription = string.Join(',', parts.Skip(1)).Trim();
        }
        else
        {
            productName = productNameDescription?.Trim();
        }
        if (string.IsNullOrEmpty(productName))
            return;                             //TODO ебануть эксепшн надо везде вместо ретурна

        var tableContainer = document.QuerySelector(".container .row.mb-3");
        var quantityElement = tableContainer?.QuerySelector(".font-weight-bold");

        int productQuantity = 0;
        if (quantityElement != null)
        {
            var quantityText = quantityElement.TextContent.Trim();
            var match = Regex.Match(quantityText, @"\d+");
            if (match.Success)
            {
                if (int.TryParse(match.Value, out int quantity))
                {
                    productQuantity = quantity;
                }
            }
        }
        float productPrice = 0;
        var priceTable = tableContainer?.QuerySelector(".text-monospace");        
        if (priceTable != null)
        {
            var pricesText = priceTable.TextContent.Trim();
            var regex = new Regex(@"(\d+\.\d+)\sруб\.");
            var matches = regex.Matches(pricesText);

            if(matches.Count > 0)
            {
                var lastPriceString = matches.Last();
                var priceString = lastPriceString.Groups[1].Value;
                if (float.TryParse(priceString, out float price))
                    productPrice = price;
            }
        }

        var product = new ProductDto()
        {
            CategoryId = category.Id,
            Name = productName ?? "",
            Description = productDescription,
            Characteristics = new List<CharacteristicDto>()
        };

        var characteristicTable = document.QuerySelector(".table-responsive");

        string characteristicName = string.Empty;
        string characteristicValue = string.Empty;
        if (characteristicTable != null)
        {
            var rows = characteristicTable.QuerySelectorAll("tr");            
            foreach (var row in rows)
            {
                var cells = row.QuerySelectorAll("td");
                if (cells.Length == 2)
                {
                    characteristicName = cells[0].TextContent.Trim();
                    characteristicValue = cells[1].TextContent.Trim();
                }
                var characteristic = new CharacteristicDto()
                {
                    Name = characteristicName,
                    Value = characteristicValue
                };
                product.Characteristics.Add(characteristic);
                // if (!existingCharacteristicList.Any(c => c.Name == characteristicName))
                //     await _kafkaCharacteristicHandler.SendRequestAndWaitForResponseAsync(new Characteristic() { 
                //         Product = existingProduct,
                //         Name = characteristicName,
                //         Value = characteristicValue
                //     });
                    
            } 
        }
        var productLink = await PostgreSQLHelper.DatabaseService.GetProductLinkByLinkAsync(productUrl);
        if (productLink == null)
            await _kafkaProductLinkHandler.SendRequestAndWaitForResponseAsync(new ProductLinkDto() {
                Link = productUrl,
                Price = productPrice,
                Product = product,
                Quantity = productQuantity,
                SiteName = "Platan"
            });        
    }
}