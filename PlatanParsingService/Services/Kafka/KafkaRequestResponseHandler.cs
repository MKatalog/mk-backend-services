using Confluent.Kafka;
using Newtonsoft.Json;

namespace PlatanParsingService.Services.Kafka;
public class KafkaRequestResponseHandler<TRequest, TResponse>
{
    private readonly string _bootstrapServers;
    private readonly string _requestTopic;
    private readonly string _responseTopic;

    private readonly IProducer<Null, string> _producer;
    private readonly IConsumer<Null, string> _consumer;
    public KafkaRequestResponseHandler(string bootstrapServers,
        string requestTopic,
        string responseTopic)
    {
        _bootstrapServers = bootstrapServers;
        _requestTopic = requestTopic;
        _responseTopic = responseTopic;

        var producerConfig = new ProducerConfig { BootstrapServers = _bootstrapServers };
        _producer = new ProducerBuilder<Null, string>(producerConfig).Build();
        var consumerConfig = new ConsumerConfig { 
            BootstrapServers = _bootstrapServers,
            GroupId = $"{_responseTopic}-motherfuckerFromPlatan",
            AutoOffsetReset = AutoOffsetReset.Earliest
        };
        _consumer = new ConsumerBuilder<Null, string>(consumerConfig).Build();
    }
    ~KafkaRequestResponseHandler()
    {
        _producer.Dispose();
        _consumer.Dispose();
    }
    public async Task SendRequestAndWaitForResponseAsync(TRequest requestMessage)
    {
        var serializedRequestMessage = JsonConvert.SerializeObject(requestMessage);
        
        await _producer.ProduceAsync(_requestTopic, new Message<Null, string> { Value = serializedRequestMessage });
        _producer.Flush(); //TODO мб можно будет в будущем поставить предельное время отправки сообщения

        TResponse response = default;

        // _consumer.Subscribe(_responseTopic);
        
        // try
        // {
        //     var responseMessage = _consumer.Consume();
        //     response = JsonConvert.DeserializeObject<TResponse>(responseMessage.Message.Value);
        // }
        // catch (OperationCanceledException ex)
        // {
        //     throw ex;//TODO понять че делать в случае исключения
        // }
        // return response;
    }
}