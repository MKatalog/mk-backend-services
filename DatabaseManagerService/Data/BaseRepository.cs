using DatabaseManagerService.Data.Contexts;
using DatabaseManagerService.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using MKatalog.Abstractions.Models;

namespace DatabaseManagerService.Data;
public abstract class BaseRepository<T> : IRepository<T> where T : class
{
    private readonly PostgresContext _context;
    private readonly DbSet<T> _dbSet;

    public BaseRepository(PostgresContext context)
    {
        _context = context;
        _dbSet = context.Set<T>();
    }

    public virtual async Task<T> CreateAsync(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));
        
        await _dbSet.AddAsync(entity);
        await _context.SaveChangesAsync();        
        return entity;
    }

    public virtual async Task DeleteAsync(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));
        
        _dbSet.Remove(entity);
        await _context.SaveChangesAsync();
    }

    public void Dispose()
    {
        _context.Dispose();
    }

    public virtual async Task<IList<T>> GetAllAsync()
    {
        return await _dbSet.ToListAsync();
    }

    public virtual async Task<T> GetByIdAsync(Guid id)
    {
        return await _dbSet.FindAsync(id) ?? throw new ArgumentNullException(nameof(T));
    }

    public virtual async Task<T> UpdateAsync(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));
        
        _dbSet.Update(entity);
        await _context.SaveChangesAsync();
        return entity;
    }

    public virtual async Task<T> CreateOrUpdateAsync(T entity)
    {
        return await CreateAsync(entity);
    }
}