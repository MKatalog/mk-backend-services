using DatabaseManagerService.Data.Contexts;
using MKatalog.Abstractions.Models;

namespace DatabaseManagerService.Data.Repositories;
public class CategoryRepository : BaseRepository<Category>
{
    public CategoryRepository(PostgresContext context) : base(context)
    {
    }

    public override async Task<Category> CreateOrUpdateAsync(Category category)
    {
        if (category == null)
            throw new ArgumentNullException(nameof(category));

        return await base.CreateAsync(category);
        
        // var existingCategory = await _dbSet.FirstOrDefaultAsync(c => c.Name == category.Name);
        
        // if (existingCategory != null)
        // {
        //     return existingCategory;
        // }
        // else
        // {
        //     return await base.CreateAsync(category);
        // }
    }
}