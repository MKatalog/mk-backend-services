using DatabaseManagerService.Data.Contexts;
using MKatalog.Abstractions.Models;

namespace DatabaseManagerService.Data.Repositories;
public class ProductRepository : BaseRepository<Product>
{
    public ProductRepository(PostgresContext context) : base(context)
    {
    }
}