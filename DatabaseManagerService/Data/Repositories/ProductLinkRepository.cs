using DatabaseManagerService.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using MKatalog.Abstractions.Models;

namespace DatabaseManagerService.Data.Repositories;
public class ProductLinkRepository : BaseRepository<ProductLink>
{
    public ProductLinkRepository(PostgresContext context) : base(context)
    {
    }

    public override async Task<ProductLink> CreateOrUpdateAsync(ProductLink productLink)
    {
        if (productLink == null)
            throw new ArgumentNullException(nameof(productLink)); 
        
        var existingProductLink = await _dbSet.FirstOrDefaultAsync(c => c.Link == productLink.Link);
        
        if (existingProductLink != null && ( 
            existingProductLink.Price != productLink.Price ||
            existingProductLink.Quantity != productLink.Quantity))
        {
            existingProductLink.Quantity = productLink.Quantity;
            existingProductLink.Price = productLink.Price;
            return await base.UpdateAsync(existingProductLink);
        }
        return await base.CreateAsync(productLink);
    }
}