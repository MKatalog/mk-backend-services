using DatabaseManagerService.Data.Contexts;
using MKatalog.Abstractions.Models;

namespace DatabaseManagerService.Data.Repositories;
public class CharacteristicRepository : BaseRepository<Characteristic>
{
    public CharacteristicRepository(PostgresContext context) : base(context)
    {
    }
}