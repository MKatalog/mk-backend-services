namespace DatabaseManagerService.Data.Repositories;
public interface IRepository<T> : IDisposable where T : class
{
    Task<T> GetByIdAsync(Guid id);
    Task<IList<T>> GetAllAsync();
    Task<T> CreateAsync(T entity);
    Task<T> CreateOrUpdateAsync(T entity);
    Task<T> UpdateAsync(T entity);
    Task DeleteAsync(T entity);
}