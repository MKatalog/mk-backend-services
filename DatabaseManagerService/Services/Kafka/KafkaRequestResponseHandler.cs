using Confluent.Kafka;
using DatabaseManagerService.Data.Repositories;
using DotNetEnv;
using Newtonsoft.Json;

// request - входящие данные
// response - данные из БД. отправляются обратно парсеру

namespace DatabaseManagerService.Services.Kafka;
public class KafkaRequestResponseHandler<T>
    where T : class
{
    private readonly string _bootstrapServers;
    private readonly string _requestTopic;
    private readonly string _responseTopic;
    private readonly IRepository<T> _repository;
    private readonly IProducer<Null, string> _producer;
    private readonly IConsumer<Null, string> _consumer;

    public KafkaRequestResponseHandler(
        IRepository<T> repository)
    {
        _bootstrapServers = Env.GetString("KAFKA_BOOTSTRAP_SERVERS");
        _requestTopic = Env.GetString($"KAFKA_{typeof(T).Name.ToUpper()}_REQUEST_TOPIC");
        _responseTopic = Env.GetString($"KAFKA_{typeof(T).Name.ToUpper()}_RESPONSE_TOPIC");
        _repository = repository;

        var producerConfig = new ProducerConfig { BootstrapServers = _bootstrapServers };
        _producer = new ProducerBuilder<Null, string>(producerConfig).Build();
        var consumerConfig = new ConsumerConfig { 
            BootstrapServers = _bootstrapServers,
            GroupId = $"{_responseTopic}-motherfuckerFromDbManager",
            AutoOffsetReset = AutoOffsetReset.Earliest
        };
        _consumer = new ConsumerBuilder<Null, string>(consumerConfig).Build();
    }
    public async Task ReceiveRequestAndProcessEntityAsync()
    {
        T request = default;
        
        _consumer.Subscribe(_requestTopic);
        
        try
        {
            while(true)
            {
                var requestMessage = _consumer.Consume();
                request = JsonConvert.DeserializeObject<T>(requestMessage.Message.Value);

                var entity = await _repository.CreateOrUpdateAsync(request);
            }
        }
        catch (ConsumeException e)
        {
            Console.WriteLine($"Error occurred: {e.Error.Reason}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }
    }

    private async Task SendResponseAsync(T TEntity)
    {
        var serializedEntity = JsonConvert.SerializeObject(TEntity);

        try
        {
            var responseMessage = new Message<Null, string> { Value = serializedEntity};
            await _producer.ProduceAsync(_responseTopic, responseMessage);
        }
        catch (ProduceException<Null, string> e)
        {
            Console.WriteLine($"Failed deliver: {e.Error.Reason}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"ХЗ че за ошибка во время отправки смскм: {ex.Message}");
        }
    }
}