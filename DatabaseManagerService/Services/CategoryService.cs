using DatabaseManagerService.Data.Repositories;
using MKatalog.Abstractions.Models;

namespace DatabaseManagerService.Services;
public class CategoryService
{
    private readonly IRepository<Category> _categoryRepository;
    public CategoryService(IRepository<Category> categoryRepository)
    {
        _categoryRepository = categoryRepository;
    }

    public async Task<Category> GetCategoryByNameAsync(string categoryName)
    {
        var allCategories = await _categoryRepository.GetAllAsync();
        var category = allCategories.FirstOrDefault(c => c.Name == categoryName);

        return category;
    }

    public async Task<Category> CreateAsync(Category newCategory)
    {
        var category = await _categoryRepository.CreateAsync(newCategory);
        return category;
    }
}   