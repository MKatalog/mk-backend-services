using DatabaseManagerService.Data.Repositories;
using MKatalog.Abstractions.Models;

namespace DatabaseManagerService.Services;
public class ProductLinkService
{
    private readonly IRepository<ProductLink> _productLinkRepository;
    public ProductLinkService(IRepository<ProductLink> productLinkRepository)
    {
        _productLinkRepository = productLinkRepository;   
    }

    public async Task<Guid> CreateOrUpdateProductLinkAsync(ProductLink newProductLink)
    {
        // var existingProductLink = _productLinkRepository.;
        
        return Guid.Empty;
    }
}