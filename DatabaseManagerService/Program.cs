using DatabaseManagerService.Data.Contexts;
using DatabaseManagerService.Data.Repositories;
using DatabaseManagerService.Services.Kafka;
using DotNetEnv;
using Microsoft.EntityFrameworkCore;
using MKatalog.Abstractions.Models;

var builder = WebApplication.CreateBuilder(args);

Env.Load(builder.Configuration.GetValue<string>("EnvFilePath"));
// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<PostgresContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("Postgres"))
        .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole())),
    ServiceLifetime.Scoped);

builder.Services.AddScoped<IRepository<Category>, CategoryRepository>();
builder.Services.AddScoped<IRepository<Product>, ProductRepository>();
builder.Services.AddScoped<IRepository<ProductLink>, ProductLinkRepository>();
builder.Services.AddScoped<IRepository<Characteristic>, CharacteristicRepository>();
    
var app = builder.Build();

// app.Use(async (context, next) =>
// {
//     var kafkaHandler = new KafkaRequestResponseHandler<Category>(
//         context.RequestServices.GetRequiredService<IRepository<Category>>());
    
//     await kafkaHandler.WaitRequestAndSendAnswerAsync();
    
//     await next();
// });

// app.Use(async (context, next) =>
// {
//     var kafkaHandler = new KafkaRequestResponseHandler<Product>(
//         context.RequestServices.GetRequiredService<IRepository<Product>>());
    
//     await kafkaHandler.WaitRequestAndSendAnswerAsync();
    
//     await next();
// });

app.Use(async (context, next) =>
{
    var kafkaHandler = new KafkaRequestResponseHandler<ProductLink>(
        context.RequestServices.GetRequiredService<IRepository<ProductLink>>());
    
    await kafkaHandler.WaitRequestAndSendAnswerAsync();
    
    await next();
});

// app.Use(async (context, next) =>
// {
//     var kafkaHandler = new KafkaRequestResponseHandler<Characteristic>(
//         context.RequestServices.GetRequiredService<IRepository<Characteristic>>());
    
//     await kafkaHandler.WaitRequestAndSendAnswerAsync();
    
//     await next();
// });
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.Run();