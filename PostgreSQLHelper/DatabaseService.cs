﻿using DotNetEnv;
using Microsoft.EntityFrameworkCore;
using MKatalog.Abstractions.Models;
using PostgreSQLHelper.Data.Contexts;

namespace PostgreSQLHelper;
public static class DatabaseService
{
    private static readonly PostgresContext _context = new();

    public static PostgresContext GetPostgresContext()
    {
        return _context;
    }
    
    public static async Task<Category?> GetCategoryByNameAsync(string name)
    {
        var existingCategory = await _context.Categories.FirstOrDefaultAsync(c => c.Name == name);
        return existingCategory;
    }
    public static async Task<Product?> GetProductByNameAsync(string name)
    {
        var existingProduct = await _context.Products.FirstOrDefaultAsync(c => c.Name == name);
        return existingProduct;
    }
    public static async Task<Product?> GetProductByIdAsync(Guid productId)
    {
        var existingProduct = await _context.Products.FindAsync(productId);
        return existingProduct;
    }
    public static async Task<ProductLink?> GetProductLinkByLinkAsync(string link)
    {
        var existingProductLink = await _context.ProductLinks.FirstOrDefaultAsync(c => c.Link == link);
        return existingProductLink;
    }
    // public static async Task<List<Characteristic>> GetCharacteristicListByProductIdAsync(Guid productId)
    // {
    //     var existingCharateristicList = await _context.Characteristics.Where(c => c.ProductId == productId).ToListAsync();
    //     return existingCharateristicList;
    // }
}