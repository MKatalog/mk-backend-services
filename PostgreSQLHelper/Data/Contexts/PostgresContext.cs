using DotNetEnv;
using Microsoft.EntityFrameworkCore;
using MKatalog.Abstractions.Models;

namespace PostgreSQLHelper.Data.Contexts;
public class PostgresContext : DbContext
{
    public DbSet<Product> Products { get; set; } = null!;
    public DbSet<ProductLink> ProductLinks { get; set; } = null!;
    public DbSet<Category> Categories { get; set; } = null!;
    public DbSet<Characteristic> Characteristics { get; set; } = null!;

    public PostgresContext()
    {
        Env.Load("D:\\backend\\mk-backend-services\\PostgreSQLHelper\\.env");
    }
    protected override void OnConfiguring(DbContextOptionsBuilder modelBuilder)
    {
        var connectionString = Env.GetString("DATABASE_CONNECTION_STRING");
        modelBuilder.UseNpgsql(connectionString);
    }
}