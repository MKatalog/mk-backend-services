using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace MKatalog.Abstractions.Models;
public class Characteristic : IEntity
{
    [Required]
    [Key]
    public Guid Id { get; set; }

    [Required]
    public string Name { get; set; } = string.Empty;

    [Required]
    public string Value { get; set; } = string.Empty;

    [JsonIgnore]
    public Product Product { get; set; } = new();

    // [Required]
    // public Guid ProductId { get; set; } = Guid.Empty;
}