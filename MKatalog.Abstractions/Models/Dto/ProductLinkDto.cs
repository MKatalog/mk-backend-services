using System.ComponentModel.DataAnnotations;

namespace MKatalog.Abstractions.Models.Dto;
public class ProductLinkDto
{
    public Guid Id { get; set; }

    [Required]
    public string SiteName { get; set; } = string.Empty;

    [Required]
    public string Link { get; set; } = string.Empty;

    public float Price { get; set; }

    public int Quantity { get; set; }

    [Required]
    public ProductDto Product { get; set; }
}