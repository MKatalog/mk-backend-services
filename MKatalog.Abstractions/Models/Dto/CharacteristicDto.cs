using System.ComponentModel.DataAnnotations;

namespace MKatalog.Abstractions.Models.Dto;
public class CharacteristicDto
{
    [Required]
    public string Name { get; set; } = string.Empty;

    [Required]
    public string Value { get; set; } = string.Empty;
}