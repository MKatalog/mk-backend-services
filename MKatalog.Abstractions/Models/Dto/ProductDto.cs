using System.ComponentModel.DataAnnotations;

namespace MKatalog.Abstractions.Models.Dto;
public class ProductDto
{
    public Guid Id { get; set; }

    [Required]
    public string Name { get; set; } = string.Empty;

    public string? Image { get; set; }

    public string? Description { get; set; }

    public IList<CharacteristicDto>? Characteristics { get; set; }

    public Guid CategoryId { get; set; }
}