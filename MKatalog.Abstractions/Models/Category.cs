using System.ComponentModel.DataAnnotations;

namespace MKatalog.Abstractions.Models;
public class Category : IEntity
{
    [Required]
    [Key]
    public Guid Id { get; set; }

    public Guid? ParentId { get; set; }

    [Required]
    public string Name { get; set; } = string.Empty;
}   