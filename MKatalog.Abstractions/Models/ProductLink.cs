using System.ComponentModel.DataAnnotations;

namespace MKatalog.Abstractions.Models;
public class ProductLink : IEntity
{
    [Required]
    [Key]
    public Guid Id { get; set; }

    [Required]
    public string SiteName { get; set; } = string.Empty;

    [Required]
    public string Link { get; set; } = string.Empty;

    public float Price { get; set; }

    public int Quantity { get; set; }

    [Required]
    public Product Product { get; set; } = new();

    public Guid ProductId { get; set; }
}