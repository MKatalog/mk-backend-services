using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace MKatalog.Abstractions.Models;
public class Product : IEntity
{
    [Key]
    [Required]
    public Guid Id { get; set; }

    [Required]
    public string Name { get; set; } = string.Empty;

    public string? Image { get; set; }

    public string? Description { get; set; }
    
    public IList<Characteristic>? Characteristics { get; set; }

    [Required]
    public Category Category { get; set; }

    public Guid CategoryId { get; set; }
    
    [JsonIgnore]
    public IList<ProductLink>? ProductLinks { get; set; }
}