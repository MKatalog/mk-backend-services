using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MKatalog.Abstractions.Models;
using MKatalog.Data.ModelsDto;
using MKatalog.Data.Repositories;

namespace MKatalog.Controllers;

[Route("api/product")]
[ApiController]
public class ProductController : ControllerBase
{
    private readonly IRepository<Product> _productRepository;
    private readonly IRepository<Category> _categoryRepository; 
    private readonly IMapper _mapper;

    public ProductController(
        IRepository<Product> productRepository,
        IRepository<Category> categoryRepository,
        IMapper mapper)
    {
        _productRepository = productRepository;
        _categoryRepository = categoryRepository;
        _mapper = mapper;
    }
    [HttpGet]
    [Route("id")]
    public async Task<ProductResponseDto?> GetProductAsync(Guid input)
    {
        var product = await _productRepository.GetAll()
            .Include(c => c.ProductLinks)
            .Include(c => c.Characteristics)
            .Include(c => c.Category)
            .FirstOrDefaultAsync(c => c.Id == input);
        
        if (product == null)
            return null;
        
        var productDto = _mapper.Map<ProductDto>(product);
        var breadCrumbs = await GetBreadCrumbsAsync(product.Category);
        var characteristicDtoList = new List<CharacteristicDto>();
        foreach (var ch in product.Characteristics ?? [])
        {
            characteristicDtoList.Add(new CharacteristicDto()
            {
                Label = ch.Name,
                Value = ch.Value
            });
        }
        var productResponseDto = new ProductResponseDto
        {
            Id = product.Id,
            Name = product.Name,
            Image = product.Image,
            Characteristics = characteristicDtoList,
            ProductLinks = productDto.ProductLinks ?? [],
            BreadCrumbs = breadCrumbs
        };
        return productResponseDto;
    }

    [HttpGet]
    [Route("products")]
    public async Task<List<ProductResponseInListDto>> GetAllProductsAsync(ProductRequestInListDto input)
    {
        var products = await _productRepository.GetAll().ToListAsync();
        return null;
    }
    
    private async Task<List<BreadCrumbDto>> GetBreadCrumbsAsync(Category category)
    {
        var breadCrumbs = new List<BreadCrumbDto>();
        var currentCategory = category;
        
        while (currentCategory != null)
        {
            breadCrumbs.Add(new BreadCrumbDto()
            {
                Name = currentCategory.Name,
                CategoryId = currentCategory.Id
            });
            currentCategory = await _categoryRepository.GetAll().FirstOrDefaultAsync(c => c.Id == currentCategory.ParentId);
        }
        breadCrumbs.Reverse();

        int k = 1;
        foreach (var crumb in breadCrumbs)
        {
            crumb.Sequence = k;
            k++;
        }
        return breadCrumbs;
    }
}