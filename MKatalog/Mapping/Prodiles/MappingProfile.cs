using AutoMapper;
using MKatalog.Abstractions.Models;
using MKatalog.Data.ModelsDto;

namespace MKatalog.Mapping.Profiles;
public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Product, ProductDto>();
        CreateMap<ProductDto, Product>();

        CreateMap<Characteristic, CharacteristicDto>();
        CreateMap<CharacteristicDto, Characteristic>();
        
        CreateMap<ProductLink, ProductLinkDto>();
        CreateMap<ProductLinkDto, ProductLink>();

        CreateMap<Category, CategoryDto>();
        CreateMap<CategoryDto, Category>();
    }
}