using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MKatalog.Data.Contexts;
using MKatalog.Data.Repositories;

namespace MKatalog.Data;
public abstract class BaseRepository<T> : IRepository<T> where T : class
{
    private readonly PostgresContext _context;
    private readonly DbSet<T> _dbSet;
    public BaseRepository(PostgresContext context)
    {
        _context = context;
        _dbSet = _context.Set<T>();
    }
    public void Dispose() => _context.Dispose();

    public async virtual Task<IList<T>> GetAllAsync()
    {
        return await _dbSet.ToListAsync();
    }

    public async virtual Task<T> GetByIdAsync(Guid id)
    {
        return await _dbSet.FindAsync(id) ?? throw new ArgumentNullException(nameof(T));
    }
    
    public virtual IQueryable<T> GetAll()
    {
        return _dbSet;
    }
}