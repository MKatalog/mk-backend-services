namespace MKatalog.Data.Repositories;
public interface IRepository<T> : IDisposable where T : class
{
    Task<T> GetByIdAsync(Guid id);
    Task<IList<T>> GetAllAsync();
    IQueryable<T> GetAll();
}