using MKatalog.Abstractions.Models;
using MKatalog.Data.Contexts;

namespace MKatalog.Data.Repositories;
public class ProductRepository : BaseRepository<Product>
{
    public ProductRepository(PostgresContext context) : base(context)
    {
    }
}