using MKatalog.Abstractions.Models;
using MKatalog.Data.Contexts;

namespace MKatalog.Data.Repositories;
public class CategoryRepository : BaseRepository<Category>
{
    public CategoryRepository(PostgresContext context) : base(context)
    {
    }
}