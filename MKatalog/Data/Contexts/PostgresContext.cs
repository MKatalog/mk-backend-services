using Microsoft.EntityFrameworkCore;
using MKatalog.Abstractions.Models;

namespace MKatalog.Data.Contexts;
public class PostgresContext : DbContext
{
    public DbSet<Product> Products { get; set; } = null!;
    public DbSet<Category> Categories { get; set; } = null!;
    public DbSet<Characteristic> Characteristics { get; set; } = null!;
    public DbSet<ProductLink> ProductLinks { get; set; } = null!;
    public PostgresContext(DbContextOptions<PostgresContext> options) : base(options)
    {
        
    }
}