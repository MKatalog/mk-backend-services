namespace MKatalog.Data.ModelsDto;
public class CategoryDto
{
    public Guid Id { get; set; }

    public Guid? ParentId { get; set; }

    public string Name { get; set; } = string.Empty;
}