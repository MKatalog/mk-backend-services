namespace MKatalog.Data.ModelsDto;

public class ProductRequestInListDto
{
    public string ProductName { get; set; } = string.Empty;
    public int TotalCount { get; set; }
    public int Page { get; set; }
    public string? CategoryId { get; set; }
    public List<FilterDto>? Filters { get; set; }
    public SortType? Sort { get; set; }
}

public class ProductResponseInListDto
{
    public IList<ProductDataDto> Data { get; set; } = [];

    public IList<FilterResponseDto> Filters { get; set; } = [];

    public IList<BreadCrumbDto> BreadCrumbDto { get; set; }  = [];

    public int Total { get; set; }
}

public class ProductDataDto
{
    public Guid Id { get; set; }

    public string Name { get; set; } = string.Empty;

    public string ProductType { get; set; } = string.Empty; //TODO узнать что это вообще такое

    public string ImagePath { get; set; } = string.Empty;

    public int ProductLinksCount { get; set; }

    public ProductPricesDto Prices { get; set; } = new();

    public ProductQuantitiesDto Quantities { get; set; } = new();

    public ProductDeliveryTimesDto DeliveryTimes { get; set; } = new();
}

public class ProductPricesDto
{
    public float From { get; set; }

    public float To { get; set; }
}

public class ProductQuantitiesDto
{
    public int From { get; set; }

    public int To { get; set; }
}

public class ProductDeliveryTimesDto
{
    public string From { get; set; } = string.Empty;

    public string To { get; set; } = string.Empty;
}

public class FilterDto
{
    public FilterType Type { get; set; }

    public FilterBetweenAndValueDto? Between { get; set; }

    public string? Equal { get; set; }

    public IList<string>? Contains { get; set; }

}

public class FilterResponseDto : FilterDto
{
    public string Title { get; set; } = string.Empty;
    public IList<FilterBetweenAndValueDto>? Values { get; set; }
}

public class FilterBetweenAndValueDto
{
    public string From { get; set; } = string.Empty;

    public string To { get; set; } = string.Empty;
}

public enum FilterType
{
    Category,
    Price,
    DeliveryTimes,
    Quantitity,
    Producer
}

public enum SortType
{
    Price,
    Quantity,
    DeliveryTime
}