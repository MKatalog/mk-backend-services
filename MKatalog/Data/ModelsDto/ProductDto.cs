namespace MKatalog.Data.ModelsDto;
public class ProductDto
{
    public Guid Id { get; set; }

    public string Name { get; set; } = string.Empty;

    public string? Image { get; set; }

    public string? Description { get; set; }
    
    public IList<CharacteristicDto>? Characteristics { get; set; }

    public CategoryDto Category { get; set; } = new();

    public Guid CategoryId { get; set; }
    
    public IList<ProductLinkDto>? ProductLinks { get; set; }
}