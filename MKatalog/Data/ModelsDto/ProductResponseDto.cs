namespace MKatalog.Data.ModelsDto;
public class ProductResponseDto
{
    public Guid Id { get; set; } = Guid.Empty;
    
    public string Name { get; set; } = string.Empty;

    public string? Image { get; set; }

    public IList<CharacteristicDto> Characteristics { get; set; } = [];

    public IList<ProductLinkDto> ProductLinks { get; set; } = [];

    public IList<BreadCrumbDto> BreadCrumbs { get; set; } = [];
}

public class BreadCrumbDto
{
    public string Name { get; set; } = string.Empty;

    public Guid CategoryId { get; set; } = Guid.Empty;

    public int Sequence { get; set; }
}
public class CharacteristicDto
{
    public string Label { get; set; } = string.Empty;

    public string Value { get; set; } = string.Empty;
}