namespace MKatalog.Data.ModelsDto;
public class ProductLinkDto
{
    public Guid Id { get; set; }

    public string SiteName { get; set; } = string.Empty;

    public string Link { get; set; } = string.Empty;

    public float Price { get; set; }

    public int Quantity { get; set; }

    public Guid ProductId { get; set; }
}